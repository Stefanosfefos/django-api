"""Rest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

# from . import views
# from service.views import index, register,getStories,login,home
from knox import views as knox_views
from service.views import login,logout,postStory,getStories,deleteStory#LoginAPI,#,RegisterAPI,AuthorList,l
from rest_framework.urlpatterns import format_suffix_patterns

# from django.contrib.auth import views as auth_views
# urlpatterns = [
#     path('', views.index, name='index'),
# ]
urlpatterns = [
    path('admin/', admin.site.urls),
    # path('authors/',  AuthorList.as_view()),
    # path ('api/register/' , register ),
    # path ('api/list/' , HandleListRequest ),
    # path('getStories/',getStories),
    # path('', index, name='index
    path('api/login/',login,name='login'),
    path('api/postStory/',postStory,name="Post Story"),
    path('api/getstories/',getStories,name ="Get Stories"),
    path('api/deletestory/',deleteStory,name="Delete Story"),
    path('api/logout/',logout,name='logout')
    # path('api/login/',auth_views.LoginView.as_view(template_name='service/login.html'), name='login'),
    # path('api/home', home, name='home'),
    # path('api/',include('django.contrib.auth.urls')),
    # path('api/register/', RegisterAPI.as_view(), name='register'),
    # path('api/login/', LoginAPI.as_view(), name='login'),
    # path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    # path('api/logoutall/', knox_viewlines.LogoutAllView.as_view(), name='logoutall'),


    # path('<int:year>/<str:month>/', index, name='index'),
    # re_path(r'^(?P<year>[0-9]{4})/(?P<month>0?[1-9]|1[0-2])/', index, name='index'),
]
