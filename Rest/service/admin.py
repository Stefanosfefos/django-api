from django.contrib import admin
from .models import Story,Author
# from .models import CustomUser

# Register your models here.
admin.site.register(Author)
# admin.site.register(CustomUser)
admin.site.register(Story)
