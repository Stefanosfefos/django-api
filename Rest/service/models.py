from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
# from .managers import CustomUserManager
# Create your models here.

# class Author(AbstractUser):
#     name = models.CharField(max_length=30, default = "")
#     objects = CustomUserManager()
#     class Meta:
#         verbose_name = 'Author'
#         verbose_name_plural = 'Authors'
#     def __str__(self):
#         return self.name

class Author(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=30, default="")

    def __str__(self):
        return self.name


#
# class CustomAccountManager(BaseUserManager):
#     def create_user(self,username,name, password, **other_fields):
#         user = self.model(username = username,name = name,**other_fields)
#         user.set_password(password)
#         user.save()
#         return user
#
#         def create_superuser(self,username, name,password, **other_fields):
#             other_fields.setdefault('is_staff',True)
#             other_fields.setdefault('is_superuser',True)
#             other_fields.setdefault('is_active',True)
#
#             if other_fields.get('is_staff') is not True:
#                 raise ValueError(_('Superuser must have is_staff=True.'))
#                 if other_fields.get('is_superuser') is not True:
#                     raise ValueError(_('Superuser must have is_superuser=True.'))
#                     # user = self.model(username = username,name = name)
#                     # user.set_password(password)
#                     # user.save()
#                     return self.create_user(username,name,password,**other_fields)
#
#
#
#                     class Author(AbstractBaseUser, PermissionsMixin):
#                         username = models.CharField(max_length = 30, unique = True,default = "")
#                         name = models.CharField(max_length=30, default = "")
#                         email = None
#                         is_staff = models.BooleanField(default=False)
#                         is_active = models.BooleanField(default=False)
#
#                         objects = CustomAccountManager()
#
#                         USERNAME_FIELD = 'username'
#                         REQUIRED_FIELDS = ['name']

#
#
#     USERNAME_FIELD = 'username'
#
#     def __str__(self):
#         return self.username
    # password = models.CharField(max_length=100, default = "")
    # username = models.CharField(max_length = 30, unique = True,default = "")
    # name = models.CharField(max_length=30, default = "")
    # password = models.CharField(max_length=100, default = "")

    # objects = CustomUserManager()
    #
    # def __str__(self):
    #      return self.name
    # address = models.CharField(max_length=50, default = "None")
    # city = models.CharField(max_length=60, default = "None")
    # state_province = models.CharField(max_length=30, default = "None")
    # country = models.CharField(max_length=50, default = "None")
    # website = models.URLField(default = "None")



class Story(models.Model):
    headline = models.CharField(max_length = 64)
    STORY_CATEGORIES = [
                        ('pol','Politics'),
                        ('art','Art News'),
                        ('tech','Technology News'),
                        ('trivia','Trivial News'),
                       ]
    STORY_REGIONS = [('uk','UK News'),
                      ('eu','European News'),
                      ('w','World News')
                    ]
    category = models.CharField(max_length=6, choices = STORY_CATEGORIES, default = "pol")
    region = models.CharField(max_length=2, choices = STORY_REGIONS, default = "")
    author = models.ForeignKey(Author,blank=False,on_delete = models.PROTECT)
    date = models.DateField(default=timezone.now)
    details = models.CharField(max_length = 512,blank=True)

    # def __str__(self):
    #      return self.headline

# class Book(models.Model):
#     title = models.CharField(max_length=100)
#     authors = models.ManyToManyField(Author)
#     publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE,)
#     publication_date = models.DateField()
