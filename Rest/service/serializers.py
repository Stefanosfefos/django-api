from rest_framework import serializers
# from django.contrib.auth.models import User
from .models import Story,Author

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        # fields = ['name']
        fields = '__all__'

#User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'username', 'password')
        read_only_fields = ('is_active','is_staff')

# # Register Serializer
# class RegisterSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'first_name', 'password')
#         extra_kwargs = {'password': {'write_only': True}}
#
#     def create(self, validated_data):
#         user = User.objects.create_user(validated_data['username'], validated_data['first_name'], validated_data['password'])
#
#         return user
