from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt

from datetime import date
import calendar
from calendar import HTMLCalendar
from .models import Story#,Author
# Create your views here.

from rest_framework import generics, permissions, status
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import  AuthorSerializer#,RegisterSerializer,UserSerializer
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required

from datetime import datetime
from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.views import APIView
from knox.views import LoginView as KnoxLoginView
from rest_framework.authentication import SessionAuthentication
# from django.contrib.auth.models import
import json


@csrf_exempt
def login(request):
    if request.method == "POST":
        # print("post")
        try:
            username = request.POST.get('username')
            if username is None or username == "":
                resp = "Invalid data: urlencoded form 'username' field not found or empty"
                return HttpResponse(resp,status=400,content_type="text/plain")
            password = request.POST.get('password')
            if password is None or password == "":
                resp = "Invalid data: urlencoded form 'password' field not found or empty"
                return HttpResponse(resp,status=400,content_type="text/plain")
        except:
            resp = "Invalid data: urlencoded form must contain a 'username' and 'password' field"
            return HttpResponse(resp,status=400,content_type="text/plain")
        user = authenticate(request,username=username,password=password)
        resp = " "
        if user is not None:
            resp = "Welcome " + username
            auth_login(request,user)
            return HttpResponse(resp,status = 200,content_type="text/plain")
            print("user authenticated")
        else:
            resp = "User not authenticated. Invalid username or password"
            print("user not authenticated. Invalid username or password")
            return HttpResponse(resp,status=401,content_type="text/plain")
            # print("user not authenticated")
    else:
        resp = str(request.method) + " method not allowed."
        return HttpResponse(resp,status=400,content_type="text/plain")


@csrf_exempt
def logout(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            print("logging out")
            current_user = request.user

            auth_logout(request)
            resp = "Goodbye " + str(current_user.author) + " ! Hope to see you again Soon!"
            return HttpResponse(resp,status = 200,content_type="text/plain")
        else:
            resp = "Must be logged in to log out"
            print("invalid logout")
            return HttpResponse(resp,status = 401,content_type="text/plain")
        # return response
    else:
        resp = "This api URL only accepts POST requests"
        print("bad request")
        return HttpResponse(resp, status = 400,content_type="text/plain")
    # restponse.staus_code =
    # return HttpResponse(response)


@csrf_exempt
def postStory(request):
    if request.user.is_authenticated:
        if request.user.author is None:
            resp = "Only Authors can post stories. Please contact the admin to become an author"
            return HttpResponse(resp, content_type="text/plain")
        if request.method == "POST":
            print(request.body)
            if request.content_type == "application/json":
                try:
                    body = json.loads(request.body)
                    headline = body['headline']
                    category = body['category']
                    region = body['region']
                    details = body['details']
                    if len(headline) > 64:
                        resp = "Invalid data: 'headline' field must be no more than 64 characters"
                        return HttpResponse(resp,status=503,content_type="text/plain")
                    if len(details) > 512:
                        resp = "Invalid data: 'details' field must be no more than 512 characters"
                        return HttpResponse(resp,status=503,content_type="text/plain")
                except:
                    resp = "Invalid data: Json must contain a 'headline','category','region', and 'details' field"
                    return HttpResponse(resp,status=503,content_type="text/plain")
                current_user = request.user
                author = current_user.author
                print(author)
                story1 = Story(headline = headline, category = category, region = region,author = author,details = details)
                story1.save()
                print(headline)
                print(category)
                print(region)
                print(details)
                resp = "Story saved successfully"
                return HttpResponse(resp,status=201,content_type="text/plain")
            else:
                resp = "Invalid content type: must be of type: 'application/json'"
                return HttpResponse(resp,status=503,content_type="text/plain")
        else:
            resp = "This api URL only accepts POST requests"
            print("bad request")
            return HttpResponse(resp, status = 503,content_type="text/plain")

    else:
        resp = "Must be logged in to post a story"
        return HttpResponse(resp,status =503,content_type="text/plain")


@csrf_exempt
def getStories(request):
    if request.method == "GET":
        if request.content_type == "application/x-www-form-urlencoded":
            print("get")

            body = json.loads(request.body)
            try:
                category =  body['story_cat']
                story_region = body['story_region']
                story_date = body['story_date']
                print(category)
                print(story_region)
                print(story_date)
            except Exception as e:
                resp = "Invalid data: must include a 'story_cat', 'story_region' and 'story_date' field"
                return HttpResponse(resp,content_type="text/plain")


            STORY_CATEGORIES = ['pol','art','tech','trivia']
            STORY_REGIONS = ['uk','eu','w']
            any=[]
            if category != "*":
                if category not in STORY_CATEGORIES:
                    resp = "Invalid category: must be one of 'pol','art','tech','trivia' or '*'"
                    return HttpResponse(resp,status=400,content_type="text/plain")
                else:
                    any.append(1)
            else:
                any.append(0)
            if story_region != "*":
                if story_region not in STORY_REGIONS:
                    resp = "Invalid region: must be one of 'uk','eu','w', or '*'"
                    return HttpResponse(resp,status=400,content_type="text/plain")
                else:
                    any.append(1)
            else:
                any.append(0)
            if story_date != "*":
                any.append(1)
                try:
                    dt = story_date.split('/')
                    print(dt)
                    dateobj = datetime(int(dt[2]),int(dt[1]),int(dt[0]))
                    print(dateobj)
                    story_date = dateobj.strftime("%Y-%m-%d")
                except:
                    resp ="Invalid Date format"
                    return HttpResponse(resp,status=400,content_type="text/plain")
            else:
                any.append(0)
            count = 0
            for i in any:
                count+=i
            stories = Story.objects.all()
            if any[0] != 0:
                stories = stories.filter(category=category)
            if any[1] != 0:
                stories = stories.filter(region = story_region)
            if any[2] != 0:
                stories = stories.filter(date__range= [story_date,datetime.today()])
            # stories = Story.objects.filter(cate)
            # stories = Story.objects.filter(category = category,region = story_region,date = story_date)
            # stories = Story.objects.all()
            records = []
            if not stories:
                resp = "No stories found"
                return HttpResponse(resp,status=404,content_type='text/plain')
            else:
                print("stories found")
                # print(stories)
                for story in stories:
                    # ,"author":story.author
                    record = { "key": story.id,"headline":story.headline, "story_cat":story.category,"story_region":story.region,"author":str(story.author),"story_date":str(story.date),"story_details":story.details}
                    records.append(record)
                    # print(story.category)
                    # print(story.region)
                    # print(story.date)
                story_list = json.dumps({"stories":records})
                # print(story_list)
                # serializer = AuthorSerializer(stories,many=True)
                return HttpResponse(story_list,status=200,content_type="application/json")
        else:
            resp = "Invalid content type: must be of type: 'application/x-www-form-urlencoded'"
            return HttpResponse(resp,status=503,content_type="text/plain")
        # return render(request,'/service/login.html')
    else:
        resp = "This api URL only accepts POST requests"
        print("bad request")
        return HttpResponse(resp, status = 503,content_type="text/plain")

@csrf_exempt
def deleteStory(request):
    # if request.method == "GET":
    #     print("get")
        # return render(request,'/service/login.html')
    if request.method == "POST":
        if not request.user.is_authenticated:
            resp = "User not authenticated. Must be logged in to delete a story"
            return HttpResponse(resp,status =503,content_type='text/plain')
        if request.content_type == "application/json":
            body = json.loads(request.body)
            try:
                key = body['story_key']
                print(key)
                key = int(key)
                if key == None:
                    resp = "Invalid data: JSON payload must include a 'story_key' field"
                    return HttpResponse(resp,status =503,content_type='text/plain')
            except:
                resp = "Invalid data: JSON payload must include a 'story_key' field"
                return HttpResponse(resp,status =503,content_type='text/plain')

            story = Story.objects.filter(id=key)
            if not story:
                # print("no story found")
                resp = "Story doesn't exist"
                return HttpResponse(resp,status =503,content_type='text/plain')
            else:
                story.delete()
                resp = "Story deleted successfully"
                return HttpResponse(resp,status = 201,content_type='text/plain')
        else:
            resp = "Invalid content type: Must be of type 'application/json'"
            return HttpResponse(resp,status =503,content_type='text/plain')
    else:
        resp = "This api URL only accepts POST requests"
        print("bad request")
        return HttpResponse(resp, status = 503,content_type="text/plain")
